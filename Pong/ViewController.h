//
//  ViewController.h
//  Pong
//
//  Created by dronnefjord on 2015-02-14.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    BOOL gameIsPlaying;
    CGFloat X;
    CGFloat Y;
    int score;
    CGFloat speed;
}

@property (weak, nonatomic) IBOutlet UILabel *scoreText;
@property (weak, nonatomic) IBOutlet UILabel *tapToPlayText;
@property (weak, nonatomic) IBOutlet UIImageView *playerPaddle;
@property (weak, nonatomic) IBOutlet UIImageView *ball;
@property (nonatomic) NSTimer *timer;

@end

