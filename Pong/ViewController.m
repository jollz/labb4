//
//  ViewController.m
//  Pong
//
//  Created by dronnefjord on 2015-02-14.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize playerPaddle, ball;

#pragma mark - Ball Movement

-(void) update{
    if(gameIsPlaying){
        self.tapToPlayText.text = @"";
        if(ball.center.y > playerPaddle.center.y + playerPaddle.frame.size.height/2){
            gameIsPlaying = NO;
            score = 0;
            speed = 1.0f;
            [self resetBall];
            self.scoreText.text = @"";
            self.tapToPlayText.text = @"Tap to play again!";
        }
        
        if(ball.center.y < ball.frame.size.height/2){
            [self score];
            Y *= -1;
        }
        
        if(ball.center.x > self.view.frame.size.width-ball.frame.size.width/2){
            X *= -1;
        }
        
        if(ball.center.x < ball.frame.size.width/2){
            X *= -1;
        }
        
        if(CGRectIntersectsRect(playerPaddle.frame, ball.frame)){
            if(ball.center.x < playerPaddle.center.x){
                X = -1;
            } else {
                X = 1;
            }
            Y *= -1;
        }
        
        ball.center = CGPointMake(ball.center.x + X, ball.center.y + Y);
    }
}

#pragma mark - Player Movement

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch* touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self.view];
    
    if(!gameIsPlaying){
        gameIsPlaying = YES;
    } else {
        [UIView beginAnimations:nil context:nil];
        CGRect newFrame = CGRectMake(location.x-playerPaddle.frame.size.width/2, playerPaddle.frame.origin.y, playerPaddle.frame.size.width, playerPaddle.frame.size.height);
            playerPaddle.frame = newFrame;
        [UIView commitAnimations];
    }
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch* touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self.view];
    
    CGRect newFrame = CGRectMake(location.x-playerPaddle.frame.size.width/2, playerPaddle.frame.origin.y, playerPaddle.frame.size.width, playerPaddle.frame.size.height);
        playerPaddle.frame = newFrame;
}

#pragma mark - Misc Logic

-(void) score{
    score++;
    speed += 0.1f;
    
    X *= speed;
    Y *= speed;
    
    self.scoreText.text = [NSString stringWithFormat:@"%d", score];
}

-(void) placeAndScaleBall{
    CGSize screenSize = self.view.frame.size;
    CGSize ballSize = ball.frame.size;
    
    ballSize.width = screenSize.width/15;
    ballSize.height = screenSize.width/15;
    
    CGPoint ballPosition = CGPointMake(self.view.center.x-ballSize.width/2, self.view.center.y-ballSize.height/2);
    
    ball.frame = CGRectMake(ballPosition.x, ballPosition.y, ballSize.width, ballSize.height);
}

-(void) placeText{
    CGPoint center = self.view.center;
    
    self.tapToPlayText.center = CGPointMake(center.x, center.y-(self.tapToPlayText.frame.size.height*2));
    self.scoreText.center = CGPointMake(center.x, self.scoreText.frame.size.height*2);
}

-(void) placeAndScalePaddles{
    CGSize screenSize = self.view.frame.size;
    CGSize paddleSize = playerPaddle.frame.size;
    
    paddleSize.width = screenSize.width/3;
    paddleSize.height = screenSize.height/25;
    
    CGPoint playerPaddlePosition = CGPointMake(self.view.center.x - paddleSize.width/2, self.view.frame.size.height - paddleSize.height - 50);
    
    playerPaddle.frame = CGRectMake(playerPaddlePosition.x, playerPaddlePosition.y, paddleSize.width, paddleSize.height);
}

-(void) resetBall{
    CGPoint centerPos = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    
    X = (arc4random() % 2);
    Y = (arc4random() % 2);
    if(X == 0){
        X = -1;
    }
    if(Y == 0){
        Y = -1;
    }
    X *= speed;
    Y *= speed;
    
    ball.center = centerPos;
}

#pragma mark - Generated Stuff

- (void)viewDidLoad {
    [super viewDidLoad];
    gameIsPlaying = false;
    speed = 1.0f;
    score = 0;
    X = (arc4random() % 2);
    Y = (arc4random() % 2);
    if(X == 0){
        X = -1;
    }
    if(Y == 0){
        Y = -1;
    }
    X *= speed;
    Y *= speed;
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(update) userInfo:nil repeats:YES];
}

- (void)viewDidLayoutSubviews{
    [self placeText];
    [self placeAndScaleBall];
    [self placeAndScalePaddles];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
